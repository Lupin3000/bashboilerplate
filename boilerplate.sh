#!/usr/bin/env bash

# define shell options
# --------------------
#set -x
#set -v
set -e
set -u
set -f

# define magic variables
# ----------------------
declare -r VERSION="1.0.0"
declare -r FILE_NAME=$(basename "$0")
declare -r DIR_NAME=$(dirname "$0")
declare -r LOG_FILE="/var/logs/${FILE_NAME}.log"
declare -r EXECUTE_DATE=$(date +%F)
declare -r EXECUTE_TIME=$(date +%T)
declare -r -i SUCCESS=0
declare -r -i NO_ARGS=84
declare -r -i BAD_ARGS=85
declare -r -i MISSING_ARGS=86

# trap
# ----
trap '' SIGINT SIGQUIT SIGTSTP

# global functions
# ----------------
function fc_usage()
{
  printf "Usage: %s" "$FILE_NAME"
  printf " [-h] [-V]\n"
}

function fc_version()
{
  printf "Version: %s\n" "$VERSION"
}

function fc_help()
{
  fc_usage
  fc_version
  printf "\nOptional:"
  printf "\n-h\t\t: show help"
  printf "\n-V\t\t: show version\n"
}

function fc_no_args()
{
  printf "Error: no arguments supplied\n"
  exit "$NO_ARGS"
}

function fc_bad_args()
{
  printf "Error: wrong arguments supplied\n"
  fc_usage
  exit "$BAD_ARGS"
}

function fc_missing_args()
{
  printf "Error: missing arguments: \"%s\"\n" "$1"
  exit "$MISSING_ARGS"
}

# check script arguments
# ----------------------
if [ "$#" -eq 0 ]; then
	fc_no_args
fi

while getopts "hV" OPTION; do
  case "$OPTION" in
  h)
      fc_help
      exit "$SUCCESS"
      ;;
  V)
      fc_version
      exit "$SUCCESS"
      ;;
  *)
      fc_bad_args
      ;;
  esac
done

# main functions
# --------------
function main()
{
  echo "..."
}

# run
# ---
main

# exit
# ----
exit "$SUCCESS"
